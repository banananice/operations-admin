package com.group2.operations;

import com.group2.operations.service.AdminService;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

@SpringBootTest
class OperationsApplicationTests {
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	TaskService taskService;
	@Autowired
	AdminService adminService;

	@Test
	void contextLoads() {
		List<Task> tasks = taskService.createTaskQuery().taskAssignee("department").orderByTaskCreateTime().desc().list();
		for (Task task : tasks) {
			System.out.println(task.toString());
		}
	}


}
