package com.group2.operations.controller;

import com.group2.operations.common.api.ApiResult;
import com.group2.operations.mapper.FlowMapper;
import com.group2.operations.model.Admin;
import com.group2.operations.model.vo.ContractVO;
import com.group2.operations.service.AdminService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.flowable.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;


    /**
     * 管理员登录
     * @param admin 管理员
     * @return
     */
    @PostMapping("/login")
    @ApiOperation("登录")
    @ApiImplicitParam(name = "admin", dataTypeClass = Admin.class, value = "管理员对象", required = true)
    public ApiResult login(@RequestBody Admin admin) {
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        //封装用户登录数据
        UsernamePasswordToken token = new UsernamePasswordToken(admin.getUsername(), admin.getPwd());
        //封装SessionID
        Map map = new HashMap();
        //执行登陆方法
        try {
            subject.login(token);
            map.put("token", subject.getSession().getId().toString());
            if(subject.hasRole("admin_dep")) {
                map.put("role", "admin_dep");
            }
            else if(subject.hasRole("admin_co")) {
                map.put("role", "admin_co");
            }
        } catch (UnknownAccountException e) {
            return ApiResult.failed("用户名或密码错误");
        } catch (IncorrectCredentialsException e) {
            return ApiResult.failed("用户名或密码错误");
        }

        return ApiResult.success(map, "登录成功");
    }


    @GetMapping("/department/trackall")
    @RequiresPermissions("admin_dep:track")
    @ApiOperation("部门管理员查看全部需要审核的流程")
    public ApiResult departmentTrackAll(){
        Subject subject = SecurityUtils.getSubject();
        //获取用户
        Admin admin = (Admin) subject.getPrincipal();
        //获取用户名
        String username = admin.getUsername();
        return ApiResult.success(adminService.adminDepCheck(),"成功");
    }
    @PutMapping("/department/trackall/{taskId}/agree")
    @RequiresPermissions("admin_dep:track")
    @ApiOperation("部门管理员审核通过流程")
    public ApiResult departmentAgree(@PathVariable String taskId){
        Subject subject = SecurityUtils.getSubject();
        //获取用户
        Admin admin = (Admin) subject.getPrincipal();
        //获取用户名
        String username = admin.getUsername();
        String employee_num = admin.getEmployee_num();
        String action = "通过";
        return adminService.adminDepTrack(username,employee_num,taskId,action);
    }

    @PutMapping("/department/trackall/{taskId}/reject")
    @RequiresPermissions("admin_dep:track")
    @ApiOperation("部门管理员审核驳回流程")
    public ApiResult departmentReject(@PathVariable String taskId){
        Subject subject = SecurityUtils.getSubject();
        //获取用户
        Admin admin = (Admin) subject.getPrincipal();
        //获取用户名
        String username = admin.getUsername();
        String employee_num = admin.getEmployee_num();
        String action = "驳回";
        return adminService.adminDepTrack(username,employee_num,taskId,action);
    }

    @GetMapping("/corporation/trackall")
    @RequiresPermissions("admin_co:track")
    @ApiOperation("公司管理员查看全部需要审核的流程")
    public ApiResult corporationTrackAll(){
        Subject subject = SecurityUtils.getSubject();
        //获取用户
        Admin admin = (Admin) subject.getPrincipal();
        //获取用户名
        String username = admin.getUsername();
        return ApiResult.success(adminService.adminCoCheck(),"成功");
    }


    @PutMapping("/corporation/trackall/{taskId}/agree")
    @RequiresPermissions("admin_co:track")
    @ApiOperation("公司管理员审核通过流程")
    public ApiResult corporationAgree(@PathVariable String taskId){
        Subject subject = SecurityUtils.getSubject();
        //获取用户
        Admin admin = (Admin) subject.getPrincipal();
        //获取用户名
        String username = admin.getUsername();
        String employee_num = admin.getEmployee_num();
        String action = "通过";
        return adminService.adminCoTrack(username,employee_num,taskId,action);
    }

    @PutMapping("/corporation/trackall/{taskId}/reject")
    @RequiresPermissions("admin_co:track")
    @ApiOperation("公司管理员审核驳回流程")
    public ApiResult corporationReject(@PathVariable String taskId){
        Subject subject = SecurityUtils.getSubject();
        //获取用户
        Admin admin = (Admin) subject.getPrincipal();
        //获取用户名
        String username = admin.getUsername();
        String employee_num = admin.getEmployee_num();
        String action = "驳回";
        return adminService.adminCoTrack(username,employee_num,taskId,action);
    }

    @PostMapping("/corporation/contract")
    @RequiresPermissions("admin_co:track")
    @ApiOperation("为某一流程拟定合同")
    public ApiResult delegateContract(@RequestBody ContractVO vo) {
        return adminService.delegateContract(vo);
    }
}
