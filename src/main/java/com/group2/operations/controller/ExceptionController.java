package com.group2.operations.controller;

import com.group2.operations.common.api.ApiErrorCode;
import com.group2.operations.common.api.ApiResult;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler({UnauthorizedException.class, AuthorizationException.class})
    @ResponseBody
    public ApiResult unauthorized() {
        return ApiResult.failed(ApiErrorCode.FORBIDDEN);
    }
}
