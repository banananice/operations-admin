package com.group2.operations.service;

import com.group2.operations.common.api.ApiResult;
import com.group2.operations.model.Admin;
import com.group2.operations.model.vo.ContractVO;

import java.util.List;
import java.util.Map;

public interface AdminService {
    /**
     * 根据用户名查找用户
     * @param username 用户名
     * @return 查找到的用户
     */
    Admin queryUsernameByName(String username);

    /**
     * 部门管理员审核流程
     */
    ApiResult adminDepTrack(String username,String employee_num,String taskId,String action);

    /**
     * 公司管理员审核流程
     */
    ApiResult adminCoTrack(String username,String employee_num,String taskId,String action);

    /**
     * 部门管理员获取所有需要审核的流程
     * @return
     */
    List<Object> adminDepCheck();

    /**
     * 部门管理员获取所有需要审核的流程
     * @return
     */
    List<Object> adminCoCheck();

    /**
     * 为某一个流程指派合同
     * @param contract 合同对象
     * @return
     */
    ApiResult delegateContract(ContractVO contract);
}
