package com.group2.operations.service.impl;

import com.group2.operations.common.api.ApiResult;
import com.group2.operations.mapper.*;
import com.group2.operations.model.Admin;
import com.group2.operations.model.vo.ContractVO;
import com.group2.operations.service.AdminService;
import liquibase.pro.packaged.O;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private TaskService taskService;
    @Autowired
    private FlowMapper flowMapper;
    @Autowired
    private StoreMapper storeMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ContractMapper contractMapper;

    @Override
    public Admin queryUsernameByName(String username) {

        return adminMapper.queryUsernameByName(username);
    }

    @Override
    public List<Object> adminDepCheck(){
        List<Task> taskList = taskService.createTaskQuery().taskAssignee("department").list();
        List<Object> listMap = new ArrayList<>();
        for (Task task : taskList){
            Map<String, Object> variables = taskService.getVariables(task.getId());
            variables.put("taskName", task.getName());
            variables.put("taskId", task.getId());

            listMap.add(variables);
        }
        return listMap;
    }

    @Override
    public List<Object> adminCoCheck(){
        List<Task> taskList = taskService.createTaskQuery().taskAssignee("corperation").list();
        List<Object> listMap = new ArrayList<>();
        for (Task task : taskList){
            Map<String, Object> variables = taskService.getVariables(task.getId());
            variables.put("taskName", task.getName());
            variables.put("taskId", task.getId());

            listMap.add(variables);
        }
        return listMap;
    }

    @Override
    @Transactional
    public ApiResult delegateContract(ContractVO contract) {
        String username = contract.getUsername();
        String taskId = contract.getTaskId();
        String title = contract.getTitle();
        String url = contract.getUrl();

        //根据用户名获取用户ID
        int uid = userMapper.queryIdByName(username);
        //将合同信息插入数据库
        contractMapper.insertContract(url, title, uid);
        //获取流程ID
        String processId = (String) taskService.getVariables(taskId).get("processId");
        //更新流程状态信息
        flowMapper.contractUpdateFlowState(processId, "合同拟定完成", new Date());

        //完成任务
        taskService.complete(taskId);

        return ApiResult.success("指定合同成功");
    }

    @Override
    @Transactional
    public ApiResult adminDepTrack(String username,String employee_num,String taskId,String action){
        Map<String ,Object> map = taskService.getVariables(taskId);
        String processId = (String) map.get("processId");
        //通过或驳回
        Map<String, Object> variables = new HashMap<>();
        variables.put("departmentOutcome", action);
        taskService.complete(taskId, variables);

        //数据库状态更新
        String state = (action.equals("通过") ? "部门审核通过" : "部门审核驳回")+" "+"审核人员："+username+" 工号："+employee_num;
        flowMapper.adminDepInsertFlowState(processId, state, new Date());
        String msg = username+"进行了审核";


        return ApiResult.success(msg);
    }

    @Override
    @Transactional
    public ApiResult adminCoTrack(String username,String employee_num,String taskId,String action){
        Map<String ,Object> map = taskService.getVariables(taskId);
        String processId = (String) map.get("processId");
        String processName = (String) map.get("processName");
        //通过或驳回
        Map<String, Object> variables = new HashMap<>();
        variables.put("corperationOutcome", action);
        taskService.complete(taskId, variables);

        //数据库状态更新
        String state = (action.equals("通过") ? "公司审核通过" : "公司审核驳回")+" "+"审核人员："+username+" 工号："+employee_num;
        flowMapper.adminCoInsertFlowState(processId, state, new Date());
        String msg = username+"进行了审核";

        //改变数据库厅店状态
        if("通过".equals(action) && "改变厅店状态".equals(processName)) {
            int sid = (int) map.get("sid");
            boolean targetState = (boolean) map.get("targetState");
            storeMapper.changeStateByID(targetState, sid);
        }
        //数据库添加厅店信息
        if("通过".equals(action) && "添加厅店申请".equals(processName)) {
            String address = (String) map.get("address");
            String phone = (String) map.get("phone");
            String name = (String) map.get("name");
            String number = (String) map.get("number");
            Date buildTime = (Date) map.get("buildTime");

            String uname = (String) map.get("INITIATOR");
            //通过用户名拿到用户ID
            int uid = userMapper.queryIdByName(uname);
            //将厅店数据插入数据库
            storeMapper.insertStore(address, buildTime, phone, name, number, uid);
        }

        return ApiResult.success(msg);
    }
}
