package com.group2.operations.common.api;

public interface IErrorCode {
    /**
     * 错误编码：-1失败；200成功
     *
     * @return
     */
    Integer getCode();

    /**
     * 错误描述
     *
     * @return 错误描述
     */
    String getMessage();
}
