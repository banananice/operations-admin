package com.group2.operations.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcessState {
    private String id;
    private String state1;
    private Date time1;
    private String state2;
    private Date time2;
    private String state3;
    private Date time3;
    private String state4;
    private Date time4;
    private String state5;
    private Date time5;
}
