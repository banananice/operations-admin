package com.group2.operations.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

/**
 * 角色
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role implements Serializable{
    private static final long serialVersionUID = 5834977369691261371L;
    private int id;
    //角色名称，如注册用户，代理商等
    private String name;
    //权限列表
    private Set<Permission> permissions;
}
