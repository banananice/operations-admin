package com.group2.operations.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Permission implements Serializable{
    private static final long serialVersionUID = 8380912434788804366L;
    private int id;
    //权限名称
    private String name;
}
