package com.group2.operations.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Admin implements Serializable {
    private static final long serialVersionUID = 7933011760223392339L;
    private int id;
    //真实姓名
    private String name;
    //密码
    private String pwd;
    //用户名
    private String username;
    //角色
    private Role role;
    //工号
    private String employee_num;
}
