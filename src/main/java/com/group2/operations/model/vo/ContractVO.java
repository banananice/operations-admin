package com.group2.operations.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContractVO implements Serializable{
    //用户名
    private String username;
    //标题
    private String title;
    //URL
    private String url;
    //任务ID
    private String taskId;
}
