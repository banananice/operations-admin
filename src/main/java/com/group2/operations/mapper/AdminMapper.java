package com.group2.operations.mapper;

import com.group2.operations.model.Admin;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;


@Repository
@Mapper
public interface AdminMapper {

    /**
     * 根据用户名查找用户
     * @param username 用户名
     * @return 查找到的用户
     */
    @Select("select * from admin where username=#{username}")
    @Results({
            @Result(id = true, column = "id", property = "id"),
            @Result(column = "username", property = "username"),
            @Result(column = "pwd", property = "pwd"),
            @Result(column = "rid", property = "role", one = @One(select = "com.group2.operations.mapper.RoleMapper.queryRoleById"))
    })
    Admin queryUsernameByName(@Param("username") String username);
}
