package com.group2.operations.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


@Repository
@Mapper
public interface ContractMapper {
    /**
     * 插入一条合同数据
     * @param url 储存URL
     * @param title 合同标题
     * @param uid 用户ID
     */
    @Insert("insert into contract(id, url, time, signed, uid, title) " +
            " values(null, #{url}, null, 0, #{uid}, #{title})")
    void insertContract(@Param("url") String url, @Param("title") String title, @Param("uid") int uid);
}
