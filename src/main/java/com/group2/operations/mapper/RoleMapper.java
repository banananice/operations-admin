package com.group2.operations.mapper;

import com.group2.operations.model.Role;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface RoleMapper {
    /**
     * 根据id查找角色
     * @param id 角色主键
     * @return 角色实体
     */
    @Select("select * from role where id=#{id}")
    @Results({
            @Result(id = true, column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "id", property = "permissions", many = @Many(select = "com.group2.operations.mapper.PermissionMapper.queryPermissionsByRid"))
    })
    Role queryRoleById(int id);
}
