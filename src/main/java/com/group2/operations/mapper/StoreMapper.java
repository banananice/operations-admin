package com.group2.operations.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
@Mapper
public interface StoreMapper {
    /**
     * 通过厅店ID改变厅店的状态
     * @param targetState
     * @param sid
     */
    @Update("update `store` set closed=#{state} where id=#{id}")
    void changeStateByID(@Param("state") boolean targetState, @Param("id") int sid);

    /**
     * 插入厅店信息
     * @param address 地址
     * @param time 建立时间
     * @param phone 电话
     * @param name 姓名
     * @param number 电话号码
     * @param uid 用户ID
     */
    @Insert("insert into store(id, address, time, phone, name, number, closed, uid) " +
            " values(null, #{address}, #{time}, #{phone}, #{name}, #{number}, 0, #{uid})")
    void insertStore(@Param("address") String address,
                     @Param("time") Date time,
                     @Param("phone") String phone,
                     @Param("name") String name,
                     @Param("number") String number,
                     @Param("uid") int uid);
}
