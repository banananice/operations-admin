package com.group2.operations.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserMapper {
    /**
     * 根据用户名查找ID
     * @param name 用户名
     */
    @Select("select id from user where name=#{name}")
    int queryIdByName(String name);
}
