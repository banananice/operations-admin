package com.group2.operations.mapper;

import com.group2.operations.common.api.ApiResult;
import com.group2.operations.model.ProcessState;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
@Mapper
public interface FlowMapper {

    /**
     * 部门管理员track流程的记录
     * @param processId 流程
     * @param state 状态描述
     * @param time 发生时间
     * @return
     */
    @Update("update `process-state` set state2=#{state2}, time2=#{time2} where id=#{id}")
    void adminDepInsertFlowState(@Param("id") String processId, @Param("state2") String state, @Param("time2") Date time);

    /**
     * 公司管理员track流程的记录
     * @param processId 流程
     * @param state 状态描述
     * @param time 发生时间
     */
    @Update("update `process-state` set state3=#{state3}, time3=#{time3} where id=#{id}")
    void adminCoInsertFlowState(@Param("id") String processId, @Param("state3") String state, @Param("time3") Date time);

    /**
     * 合同拟定完成记录
     * @param procesId 流程ID
     * @param state 状态描述
     * @param time 时间
     */
    @Update("update `process-state` set state4=#{state4}, time4=#{time4} where id=#{id}")
    void contractUpdateFlowState(@Param("id") String procesId, @Param("state4") String state, @Param("time4") Date time);
}
