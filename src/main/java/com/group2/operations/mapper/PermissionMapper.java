package com.group2.operations.mapper;

import com.group2.operations.model.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
@Mapper
public interface PermissionMapper {
    /**
     * 根据角色id查找权限
     * @param rid 角色id
     * @return 权限实体集合
     */
    @Select("select * from permission where rid=#{rid}")
    Set<Permission> queryPermissionsByRid(int rid);
}
